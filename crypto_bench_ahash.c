#include <crypto/hash.h>
#include <linux/err.h>
#include <linux/ktime.h>
#include <linux/scatterlist.h>

#include "crypto_bench_ahash.h"
#include "crypto_bench_common.h"

static int ahash_digest(struct ahash_request *req, u64 *out_ns)
{
    struct result res;
    ktime_t time;
    int err;

    res.err = 0;
    init_completion(&res.comp);

    ahash_request_set_callback(req, CRYPTO_TFM_REQ_MAY_BACKLOG,
                               &result_complete, &res);

    time = ktime_get();
    err = crypto_ahash_digest(req);
    switch (err) {
    case 0:
        break;
    case -EINPROGRESS:
    case -EBUSY:
        wait_for_completion(&res.comp);
        err = res.err;
        if (err == 0)
            break;
        /* fall through */
    default:
        break;
    }
    *out_ns = ktime_to_ns(ktime_sub(ktime_get(), time));
    return err;
}


void benchmark_ahash_start(bool csv_output)
{
    if (csv_output) {
        pr_emerg("%s,%s,%s\n", "alg", "data_bytes", "time_ns");
    } else {
        pr_emerg("%16s%16s%16s\n", "ALGORITHM", "DATA (B)", "TIME (ns)");
    }
}

void benchmark_ahash_end(bool csv_output)
{
    pr_emerg("\n");
}

int benchmark_ahash_do(bool csv_output, const struct ahash_spec *spec,
                       unsigned int cryptlen)
{
    struct crypto_ahash *tfm;
    struct scatterlist sg[1];
    struct ahash_request *req;
    unsigned int i, digestsize;
    u8 *data, *digest, *key;
    u64 time, time_tot, samples;
    int err = 0;

    tfm = crypto_alloc_ahash(spec->name, 0, 0);
    if (IS_ERR(tfm)) {
        err = PTR_ERR(tfm);
        if (err == -ENOENT)
            err = 0;
        goto cleanup_none;
    }

    req = ahash_request_alloc(tfm, GFP_KERNEL);
    if (IS_ERR(req)) {
        err = PTR_ERR(req);
        goto cleanup_tfm;
    }

    digestsize = crypto_ahash_digestsize(tfm);

    data = alloc_random(cryptlen + digestsize + spec->keysize);
    if (data == NULL) {
        err = -ENOMEM;
        goto cleanup_req;
    }

    digest = data + cryptlen;
    key = digest + digestsize;

    if (spec->keysize) {
        err = crypto_ahash_setkey(tfm, key, spec->keysize);
        if (err) {
            goto cleanup_data;
        }
    }

    sg_init_one(sg, data, cryptlen);

    ahash_request_set_crypt(req, sg, digest, cryptlen);

    time_tot = 0;
    samples = 0;
    for (i = 0; i < BENCHMARK_SAMPLES; i++) {
        err = ahash_digest(req, &time);
        if (err) {
            goto cleanup_data;
        }
        time_tot += time;
        samples++;

        if (time_tot > BENCHMARK_MAX_NS)
            break;
    }

    time = time_tot / samples;

    if (csv_output) {
        pr_emerg("\"%s\",%u,%llu\n", spec->name, cryptlen, (long long unsigned)time);
    } else {
        pr_emerg("%16s%16u%16llu\n", spec->name, cryptlen, (long long unsigned)time);
    }

cleanup_data:
    kfree(data);
cleanup_req:
    ahash_request_free(req);
cleanup_tfm:
    crypto_free_ahash(tfm);
cleanup_none:
    return err;
}
