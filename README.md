# A Linux kernel module for benchmarking Crypto API algorithms

This project provides a simple Linux kernel module that performs benchmarks on various algorithms provided by the Linux Crypto API. The module currently supports the `skcipher`, `ahash`, and `aead` interfaces.

## Building

To build the module, you need to have the header files for the Linux kernel installed (`sudo apt-get install linux-headers-generic` on Ubuntu). Then, just run `make`.

## Usage

**WARNING**: The modules are still in development and may crash or break your machine! It is highly recommended that you only use them inside a virtual machine.

To install the module, run (as root) `./load-human.sh` (for human-friendly output) or `./load-csv.sh` (for CSV output). To remove the module, run `./unload.sh`.

To see the benchmark results, run `dmesg -l emerg -t`.
