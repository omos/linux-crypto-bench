#include <crypto/internal/aead.h>
#include <linux/err.h>
#include <linux/ktime.h>
#include <linux/scatterlist.h>

#include "crypto_bench_aead.h"
#include "crypto_bench_common.h"

static int aead_crypt(struct aead_request *req,
                      int (*func)(struct aead_request *req),
                      u64 *out_ns)
{
    struct result res;
    ktime_t time;
    int err;

    res.err = 0;
    init_completion(&res.comp);

    aead_request_set_callback(req, CRYPTO_TFM_REQ_MAY_BACKLOG,
                              &result_complete, &res);

    time = ktime_get();
    err = func(req);
    switch (err) {
    case 0:
        break;
    case -EINPROGRESS:
    case -EBUSY:
        wait_for_completion(&res.comp);
        err = res.err;
        if (err == 0)
            break;
        /* fall through */
    default:
        break;
    }
    *out_ns = ktime_to_ns(ktime_sub(ktime_get(), time));
    return err;
}

void benchmark_aead_start(bool csv_output)
{
    if (csv_output) {
        pr_emerg("%s,%s,%s,%s,%s,%s\n", "alg", "key_bits",
                 "ad_bytes", "data_bytes", "enc_ns", "dec_ns");
    } else {
        pr_emerg("%32s%8s%16s%16s%16s%16s\n", "ALGORITHM", "KEY (b)",
                 "ASSOC DATA (B)", "DATA (B)", "TIME ENC (ns)", "TIME DEC (ns)");
    }
}

void benchmark_aead_end(bool csv_output)
{
    pr_emerg("\n");
}

int benchmark_aead_do(bool csv_output, const struct aead_spec *spec,
                      unsigned int assoclen, unsigned int cryptlen)
{
    struct crypto_aead *tfm;
    struct scatterlist sg[1];
    struct aead_request *req;
    unsigned int i, k, ivsize, keysize, tagsize;
    u8 *data, *key, *iv;
    u64 time_enc, time_dec, time_enc_tot, time_dec_tot;
    u64 samples_enc, samples_dec;
    int err = 0;

    tfm = crypto_alloc_aead(spec->name, 0, 0);
    if (IS_ERR(tfm)) {
        err = PTR_ERR(tfm);
        if (err == -ENOENT)
            err = 0;
        goto cleanup_none;
    }

    req = aead_request_alloc(tfm, GFP_KERNEL);
    if (IS_ERR(req)) {
        err = PTR_ERR(req);
        goto cleanup_tfm;
    }

    ivsize = crypto_aead_ivsize(tfm);
    tagsize = crypto_aead_maxauthsize(tfm);

    err = crypto_aead_setauthsize(tfm, tagsize);
    if (err) {
        goto cleanup_req;
    }

    data = alloc_random(assoclen + cryptlen + tagsize + ivsize);
    if (data == NULL) {
        err = -ENOMEM;
        goto cleanup_req;
    }
    iv = data + assoclen + cryptlen + tagsize;

    sg_init_one(sg, data, assoclen + cryptlen + tagsize);

    aead_request_set_ad(req, assoclen);
    aead_request_set_crypt(req, sg, sg, cryptlen, iv);

    for (k = 0; k < spec->key_size_count; k++) {
        keysize = spec->key_sizes[k];
        key = alloc_random(keysize);
        if (key == NULL) {
            err = -ENOMEM;
            goto cleanup_data;
        }

        err = crypto_aead_setkey(tfm, key, keysize);
        if (err) {
            kfree(key);
            goto cleanup_data;
        }
        kfree(key);

        time_enc_tot = 0;
        samples_enc = 0;
        for (i = 0; i < BENCHMARK_SAMPLES; i++) {
            err = aead_crypt(req, crypto_aead_encrypt, &time_enc);
            if (err) {
                goto cleanup_data;
            }
            time_enc_tot += time_enc;
            samples_enc++;

            if (time_enc_tot > BENCHMARK_MAX_NS)
                break;
        }

        time_dec_tot = 0;
        samples_dec = 0;
        for (i = 0; i < BENCHMARK_SAMPLES; i++) {
            err = aead_crypt(req, crypto_aead_decrypt, &time_dec);
            if (err && err != -EBADMSG) {
                goto cleanup_data;
            }
            err = 0;
            time_dec_tot += time_dec;
            samples_dec++;

            if (time_dec_tot > BENCHMARK_MAX_NS)
                break;
        }

        time_enc = time_enc_tot / samples_enc;
        time_dec = time_dec_tot / samples_dec;

        if (csv_output) {
            pr_emerg("\"%s\",%u,%u,%u,%llu,%llu\n", spec->name, keysize * 8,
                     assoclen, cryptlen, (long long unsigned)time_enc,
                     (long long unsigned)time_dec);
        } else {
            pr_emerg("%32s%8u%16u%16u%16llu%16llu\n", spec->name, keysize * 8,
                     assoclen, cryptlen, (long long unsigned)time_enc,
                     (long long unsigned)time_dec);
        }
    }
cleanup_data:
    kfree(data);
cleanup_req:
    aead_request_free(req);
cleanup_tfm:
    crypto_free_aead(tfm);
cleanup_none:
    return err;
}
