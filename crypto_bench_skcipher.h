#ifndef CRYPTO_BENCH_SKCIPHER_H
#define CRYPTO_BENCH_SKCIPHER_H

#include "crypto_bench_cipher.h"

struct cipher_skcipher_spec {
    char format[CRYPTO_MAX_ALG_NAME];
    unsigned int ks_k, ks_q;
};

static const struct cipher_skcipher_spec ECB_DESC = { "ecb(%s)", 1, 0 };
static const struct cipher_skcipher_spec CTR_DESC = { "ctr(%s)", 1, 0 };
static const struct cipher_skcipher_spec XTS_DESC = { "xts(%s)", 2, 0 };
static const struct cipher_skcipher_spec LRW_DESC = { "lrw(%s)", 1, 16 };
static const struct cipher_skcipher_spec EME2_DESC = { "eme2(%s)", 1, 32 };

struct skcipher_spec {
    char name[CRYPTO_MAX_ALG_NAME];
    unsigned int key_sizes[MAX_KEY_SIZES];
    unsigned int key_size_count;
};

static const struct skcipher_spec CHACHA20_SPEC = { "chacha20", { 32 }, 1 };

static inline void skcipher_spec_from_cipher(
        struct skcipher_spec *spec, const struct cipher_spec *cipher,
        const struct cipher_skcipher_spec *skcipher)
{
    unsigned int k;

    snprintf(spec->name, CRYPTO_MAX_ALG_NAME, skcipher->format, cipher->name);

    spec->key_size_count = cipher->key_size_count;
    for (k = 0; k < cipher->key_size_count; k++) {
        spec->key_sizes[k] = cipher->key_sizes[k] * skcipher->ks_k
                + skcipher->ks_q;
    }
}

void benchmark_skcipher_start(bool csv_output);
void benchmark_skcipher_end(bool csv_output);

int benchmark_skcipher_do(bool csv_output, const struct skcipher_spec *spec,
                          unsigned int cryptlen);

#endif // CRYPTO_BENCH_SKCIPHER_H
