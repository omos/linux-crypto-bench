#include "crypto_bench_common.h"

char *alloc_random(size_t len)
{
    char *res = kmalloc(len, GFP_KERNEL);
    if (res != NULL) {
        get_random_bytes(res, len);
    }
    return res;
}

void result_complete(struct crypto_async_request *req, int err)
{
    struct result *res = req->data;

    if (err == -EINPROGRESS)
        return;

    res->err = err;
    complete(&res->comp);
}

