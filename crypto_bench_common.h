#ifndef CRYPTO_BENCH_COMMON_H
#define CRYPTO_BENCH_COMMON_H

#include <crypto/algapi.h>
#include <linux/types.h>

#define BENCHMARK_SAMPLES 2048
#define BENCHMARK_MAX_NS (50 * 1000 * 1000)

char *alloc_random(size_t len);

struct result {
    struct completion comp;
    int err;
};

void result_complete(struct crypto_async_request *req, int err);

#endif // CRYPTO_BENCH_COMMON_H

