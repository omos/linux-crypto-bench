#!/bin/bash

CRYPTO_BENCH_TYPES="${CRYPTO_BENCH_TYPES:-skcipher,aead,ahash}"

insmod crypto_bench.ko csv_output=1 bench_types="$CRYPTO_BENCH_TYPES" || exit 1
