#ifndef CRYPTO_BENCH_CIPHER_H
#define CRYPTO_BENCH_CIPHER_H

#include <crypto/algapi.h>

#define MAX_KEY_SIZES 8

struct cipher_spec {
    char name[CRYPTO_MAX_ALG_NAME];
    unsigned int key_sizes[MAX_KEY_SIZES];
    unsigned int key_size_count;
};

static const struct cipher_spec AES_DESC = { "aes", { 16, 24, 32 }, 3 };

#endif // CRYPTO_BENCH_CIPHER_H

