#include <linux/module.h>
#include <linux/moduleparam.h>

#include "crypto_bench_skcipher.h"
#include "crypto_bench_aead.h"
#include "crypto_bench_ahash.h"

static bool csv_output = false;
module_param(csv_output, bool, 0);
MODULE_PARM_DESC(csv_output, "Whether to print CSV output instead of a human-"
                 "readable table.");

#define MAX_BENCH_TYPES 3
static unsigned int bench_type_count = MAX_BENCH_TYPES;
static char *bench_types[MAX_BENCH_TYPES] = { "skcipher", "aead", "ahash" };
module_param_array(bench_types, charp, &bench_type_count, 0);

static const unsigned int CHUNK_SIZES[] = {
    64, 512, 4*1024, 16*1024, 32*1024
};

static const struct cipher_spec * const CIPHERS[] = {
    &AES_DESC
};

static const struct skcipher_spec * const SKCIPHERS[] = {
    &CHACHA20_SPEC
};

static const struct cipher_skcipher_spec * const SKCIPHER_MODES[] = {
    &ECB_DESC, &CTR_DESC, &XTS_DESC, &LRW_DESC, &EME2_DESC
};

static const struct aead_spec * const AEADS[] = {
    &RFC7539_SPEC, &MORUS640_SPEC, &MORUS1280_SPEC,
    &AEGIS128_SPEC, &AEGIS128L_SPEC, &AEGIS256_SPEC
};

static const struct cipher_aead_spec * const AEAD_MODES[] = {
    &GCM_DESC, &GCMSIV_DESC, &OCB_DESC
};

static const struct ahash_spec * const AHASHES[] = {
    &GHASH_DESC, &POLYVAL_DESC, &BLAKE2B_DESC
};

static void benchmark_skciphers(void)
{
    unsigned int i, j, k, chunksize;
    struct skcipher_spec spec;
    const struct cipher_spec *cipher;
    const struct cipher_skcipher_spec *skcipher;
    int err;

    benchmark_skcipher_start(csv_output);
    for (i = 0; i < ARRAY_SIZE(CHUNK_SIZES); i++) {
        chunksize = CHUNK_SIZES[i];

        for (j = 0; j < ARRAY_SIZE(SKCIPHERS); j++) {
            spec = *SKCIPHERS[j];

            err = benchmark_skcipher_do(csv_output, &spec, chunksize);
            if (err) {
                pr_warn("crypto_bench: [WARN] Error while benchmarking %s: %i!\n",
                        spec.name, err);
            }
        }

        for (j = 0; j < ARRAY_SIZE(CIPHERS); j++) {
            cipher = CIPHERS[j];
            for (k = 0; k < ARRAY_SIZE(SKCIPHER_MODES); k++) {
                skcipher = SKCIPHER_MODES[k];

                skcipher_spec_from_cipher(&spec, cipher, skcipher);

                err = benchmark_skcipher_do(csv_output, &spec, chunksize);
                if (err) {
                    pr_warn("crypto_bench: [WARN] Error while benchmarking %s: %i!\n",
                            spec.name, err);
                }
            }
        }
    }
    benchmark_skcipher_end(csv_output);
}

static void benchmark_aeads(void)
{
    unsigned int use_ad, i, j, k, chunksize;
    struct aead_spec spec;
    const struct cipher_spec *cipher;
    const struct cipher_aead_spec *aead;
    int err;

    benchmark_aead_start(csv_output);
    for (use_ad = 0; use_ad < 2; use_ad++) {
        for (i = 0; i < ARRAY_SIZE(CHUNK_SIZES); i++) {
            chunksize = CHUNK_SIZES[i];

            for (j = 0; j < ARRAY_SIZE(AEADS); j++) {
                spec = *AEADS[j];

                err = benchmark_aead_do(csv_output, &spec,
                                        use_ad ? chunksize : 0, chunksize);
                if (err) {
                    pr_warn("crypto_bench: [WARN] Error while benchmarking %s: %i!\n",
                            spec.name, err);
                }
            }

            for (j = 0; j < ARRAY_SIZE(CIPHERS); j++) {
                cipher = CIPHERS[j];
                for (k = 0; k < ARRAY_SIZE(AEAD_MODES); k++) {
                    aead = AEAD_MODES[k];

                    aead_spec_from_cipher(&spec, cipher, aead);

                    err = benchmark_aead_do(csv_output, &spec,
                                            use_ad ? chunksize : 0, chunksize);
                    if (err) {
                        pr_warn("crypto_bench: [WARN] Error while benchmarking %s: %i!\n",
                                spec.name, err);
                    }
                }
            }
        }
    }
    benchmark_aead_end(csv_output);
}

static void benchmark_ahashes(void)
{
    unsigned int i, k, chunksize;
    const struct ahash_spec *ahash;
    int err;

    benchmark_ahash_start(csv_output);
    for (i = 0; i < ARRAY_SIZE(CHUNK_SIZES); i++) {
        chunksize = CHUNK_SIZES[i];
        for (k = 0; k < ARRAY_SIZE(AHASHES); k++) {
            ahash = AHASHES[k];

            err = benchmark_ahash_do(csv_output, ahash, chunksize);
            if (err) {
                pr_warn("crypto_bench: [WARN] Error while benchmarking %s: %i!\n",
                        ahash->name, err);
            }
        }
    }
    benchmark_ahash_end(csv_output);
}

static int __init crypto_bench_module_init(void)
{
    unsigned int i;

    for (i = 0; i < bench_type_count; i++) {
        if (strcmp(bench_types[i], "skcipher") == 0)
            benchmark_skciphers();
        else if (strcmp(bench_types[i], "aead") == 0)
            benchmark_aeads();
        else if (strcmp(bench_types[i], "ahash") == 0)
            benchmark_ahashes();
        else {
            pr_err("crypto_bench: Invalid type: '%s'!\n", bench_types[i]);
            return -EINVAL;
        }
    }
    return 0;
}

static void __exit crypto_bench_module_exit(void)
{
}

module_init(crypto_bench_module_init);
module_exit(crypto_bench_module_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ondrej Mosnacek <omosnacek@gmail.com>");
MODULE_DESCRIPTION("AEAD mode tests");
