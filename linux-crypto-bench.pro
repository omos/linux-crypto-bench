TEMPLATE = lib
CONFIG -= qt
CONFIG += staticlib

DEFINES += __KERNEL__

ARCH=x86
SRC_PROJECT_PATH = $$PWD
LINUX_VERSION = $$system(uname -r)
LINUX_HEADERS_PATH = /lib/modules/$$LINUX_VERSION/build

INCLUDEPATH += $$SRC_PROJECT_PATH/include
INCLUDEPATH += $$LINUX_HEADERS_PATH/include
INCLUDEPATH += $$LINUX_HEADERS_PATH/arch/$$ARCH/include

buildmod.commands = make -C $$LINUX_HEADERS_PATH M=$$SRC_PROJECT_PATH modules
cleanmod.commands = make -C $$LINUX_HEADERS_PATH M=$$SRC_PROJECT_PATH clean
QMAKE_EXTRA_TARGETS += buildmod cleanmod

HEADERS += \
    crypto_bench_common.h \
    crypto_bench_skcipher.h \
    crypto_bench_cipher.h \
    crypto_bench_aead.h \
    crypto_bench_ahash.h

SOURCES += \
    crypto_bench_module.c \
    crypto_bench_common.c \
    crypto_bench_skcipher.c \
    crypto_bench_aead.c \
    crypto_bench_ahash.c

DISTFILES += \
    Makefile
