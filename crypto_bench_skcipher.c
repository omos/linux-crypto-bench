#include <crypto/skcipher.h>
#include <linux/err.h>
#include <linux/ktime.h>
#include <linux/scatterlist.h>

#include "crypto_bench_skcipher.h"
#include "crypto_bench_common.h"

static int skcipher_crypt(struct skcipher_request *req,
                          int (*func)(struct skcipher_request *req),
                          u64 *out_ns)
{
    struct result res;
    ktime_t time;
    int err;

    res.err = 0;
    init_completion(&res.comp);

    skcipher_request_set_callback(req, CRYPTO_TFM_REQ_MAY_BACKLOG,
                                  &result_complete, &res);

    time = ktime_get();
    err = func(req);
    switch (err) {
    case 0:
        break;
    case -EINPROGRESS:
    case -EBUSY:
        wait_for_completion(&res.comp);
        err = res.err;
        if (err == 0)
            break;
        /* fall through */
    default:
        break;
    }
    *out_ns = ktime_to_ns(ktime_sub(ktime_get(), time));
    return err;
}

void benchmark_skcipher_start(bool csv_output)
{
    if (csv_output) {
        pr_emerg("%s,%s,%s,%s,%s\n", "alg", "key_bits", "data_bytes",
                 "enc_ns", "dec_ns");
    } else {
        pr_emerg("%16s%8s%16s%16s%16s\n", "ALGORITHM", "KEY (b)", "DATA (B)",
                 "TIME ENC (ns)", "TIME DEC (ns)");
    }
}

void benchmark_skcipher_end(bool csv_output)
{
    pr_emerg("\n");
}

int benchmark_skcipher_do(bool csv_output, const struct skcipher_spec *spec,
                          unsigned int cryptlen)
{
    struct crypto_skcipher *tfm;
    struct scatterlist sg[1];
    struct skcipher_request *req;
    unsigned int i, k, ivsize, keysize;
    u8 *data, *key;
    u64 time_enc, time_dec, time_enc_tot, time_dec_tot;
    u64 samples_enc, samples_dec;
    int err = 0;

    tfm = crypto_alloc_skcipher(spec->name, 0, 0);
    if (IS_ERR(tfm)) {
        err = PTR_ERR(tfm);
        if (err == -ENOENT)
            err = 0;
        goto cleanup_none;
    }

    req = skcipher_request_alloc(tfm, GFP_KERNEL);
    if (IS_ERR(req)) {
        err = PTR_ERR(req);
        goto cleanup_tfm;
    }

    ivsize = crypto_skcipher_ivsize(tfm);

    data = alloc_random(cryptlen + ivsize);
    if (data == NULL) {
        err = -ENOMEM;
        goto cleanup_req;
    }

    sg_init_one(sg, data, cryptlen);

    skcipher_request_set_crypt(req, sg, sg, cryptlen, data + cryptlen);

    for (k = 0; k < spec->key_size_count; k++) {
        keysize = spec->key_sizes[k];
        key = alloc_random(keysize);
        if (key == NULL) {
            err = -ENOMEM;
            goto cleanup_data;
        }

        err = crypto_skcipher_setkey(tfm, key, keysize);
        if (err) {
            kfree(key);
            goto cleanup_data;
        }
        kfree(key);

        time_enc_tot = 0;
        samples_enc = 0;
        for (i = 0; i < BENCHMARK_SAMPLES; i++) {
            err = skcipher_crypt(req, crypto_skcipher_encrypt, &time_enc);
            if (err) {
                goto cleanup_data;
            }
            time_enc_tot += time_enc;
            samples_enc++;

            if (time_enc_tot > BENCHMARK_MAX_NS)
                break;
        }

        time_dec_tot = 0;
        samples_dec = 0;
        for (i = 0; i < BENCHMARK_SAMPLES; i++) {
            err = skcipher_crypt(req, crypto_skcipher_decrypt, &time_dec);
            if (err) {
                goto cleanup_data;
            }
            time_dec_tot += time_dec;
            samples_dec++;

            if (time_dec_tot > BENCHMARK_MAX_NS)
                break;
        }

        time_enc = time_enc_tot / samples_enc;
        time_dec = time_dec_tot / samples_dec;

        if (csv_output) {
            pr_emerg("\"%s\",%u,%u,%llu,%llu\n", spec->name, keysize * 8,
                     cryptlen, (long long unsigned)time_enc,
                     (long long unsigned)time_dec);
        } else {
            pr_emerg("%16s%8u%16u%16llu%16llu\n", spec->name, keysize * 8,
                     cryptlen, (long long unsigned)time_enc,
                     (long long unsigned)time_dec);
        }
    }
cleanup_data:
    kfree(data);
cleanup_req:
    skcipher_request_free(req);
cleanup_tfm:
    crypto_free_skcipher(tfm);
cleanup_none:
    return err;
}

