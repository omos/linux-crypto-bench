obj-m += crypto_bench.o

crypto_bench-objs += \
	crypto_bench_module.o \
	crypto_bench_common.o \
	crypto_bench_skcipher.o \
	crypto_bench_aead.o \
	crypto_bench_ahash.o

ifeq ($(KERNEL_BUILD),)
	ifeq ($(KERNEL_VERSION),)
		KERNEL_VERSION=$(shell uname -r)
	endif
	KERNEL_BUILD=/lib/modules/$(KERNEL_VERSION)/build
endif

all:
	make -C $(KERNEL_BUILD) M=$(PWD) modules

clean:
	make -C $(KERNEL_BUILD) M=$(PWD) clean
