#ifndef CRYPTO_BENCH_AEAD_H
#define CRYPTO_BENCH_AEAD_H

#include "crypto_bench_cipher.h"

struct cipher_aead_spec {
    char format[CRYPTO_MAX_ALG_NAME];
    unsigned int ks_k, ks_q;
};

static const struct cipher_aead_spec CCM_DESC = { "ccm(%s)", 1, 0 };
static const struct cipher_aead_spec GCM_DESC = { "gcm(%s)", 1, 0 };
static const struct cipher_aead_spec GCMSIV_DESC = { "gcmsiv(%s)", 1, 16 };
static const struct cipher_aead_spec AXTS_DESC = { "axts(%s)", 1, 16 };
static const struct cipher_aead_spec OCB_DESC = { "ocb(%s)", 1, 0 };

struct aead_spec {
    char name[CRYPTO_MAX_ALG_NAME];
    unsigned int key_sizes[MAX_KEY_SIZES];
    unsigned int key_size_count;
};

static const struct aead_spec RFC7539_SPEC = {
    "rfc7539(chacha20,poly1305)", { 32 }, 1
};
static const struct aead_spec MORUS640_SPEC = {
    "morus640", { 16 }, 1
};
static const struct aead_spec MORUS1280_SPEC = {
    "morus1280", { 16, 32 }, 2
};
static const struct aead_spec AEGIS128_SPEC = {
    "aegis128", { 16 }, 1
};
static const struct aead_spec AEGIS128L_SPEC = {
    "aegis128l", { 16 }, 1
};
static const struct aead_spec AEGIS256_SPEC = {
    "aegis256", { 32 }, 1
};

static inline void aead_spec_from_cipher(
        struct aead_spec *spec, const struct cipher_spec *cipher,
        const struct cipher_aead_spec *aead)
{
    unsigned int k;

    snprintf(spec->name, CRYPTO_MAX_ALG_NAME, aead->format, cipher->name);

    spec->key_size_count = cipher->key_size_count;
    for (k = 0; k < cipher->key_size_count; k++) {
        spec->key_sizes[k] = cipher->key_sizes[k] * aead->ks_k
                + aead->ks_q;
    }
}

void benchmark_aead_start(bool csv_output);
void benchmark_aead_end(bool csv_output);

int benchmark_aead_do(bool csv_output, const struct aead_spec *spec,
                      unsigned int assoclen, unsigned int cryptlen);

#endif // CRYPTO_BENCH_AEAD_H
