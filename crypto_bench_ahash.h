#ifndef CRYPTO_BENCH_AHASH_H
#define CRYPTO_BENCH_AHASH_H

#include <crypto/algapi.h>

struct ahash_spec {
    char name[CRYPTO_MAX_ALG_NAME];
    unsigned int keysize;
};

static const struct ahash_spec GHASH_DESC   = { "ghash", 16 };
static const struct ahash_spec POLYVAL_DESC = { "polyval", 16 };
static const struct ahash_spec BLAKE2B_DESC = { "blake2b512", 64 };

void benchmark_ahash_start(bool csv_output);
void benchmark_ahash_end(bool csv_output);

int benchmark_ahash_do(bool csv_output, const struct ahash_spec *spec,
                       unsigned int cryptlen);

#endif // CRYPTO_BENCH_AHASH_H
